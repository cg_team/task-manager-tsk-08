package ru.inshakov.tm.repository;

import ru.inshakov.tm.constant.ArgumentConst;
import ru.inshakov.tm.constant.TerminalConst;
import ru.inshakov.tm.constant.api.ICommandRepository;
import ru.inshakov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Developer info"
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Commands"
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "App version"
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "System info"
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Close App"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "Show program arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "Show program commands"
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, EXIT, ARGUMENTS, COMMANDS
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
