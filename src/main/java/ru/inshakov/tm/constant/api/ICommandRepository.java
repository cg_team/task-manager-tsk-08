package ru.inshakov.tm.constant.api;

import ru.inshakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
